function generate(){
    var quotes = {
        "- Roger var" : '“Bitcoin is amazingly transformative because it’s the first time in the entire history of the world in which anybody can now send or receive any amount of money, with anyone else, anywhere on the planet, without having to ask permission from any bank or government.”',
        "- Alan Greenspan" : '“Bitcoin is really a fascinating example of how human beings create value, and is not always rational, it is not a rational currency in that case.”',
        "- Kim Dotcom" : '“Bitcoin is a very exciting development, it might lead to a world currency. I think over the next decade it will grow to become one of the most important ways to pay for things and transfer assets.”' 
    } 

    var authors = Object.keys(quotes);

    var author = authors[Math.floor(Math.random() * authors.length)];

    var quote = quotes[author];

    document.getElementById("quote").innerHTML = quote;

    document.getElementById("author").innerHTML = author;

}
// Get the modal
var modal = document.getElementById('id01');

// When the user clicks anywhere outside of the modal, close it
window.onclick = function(event) {
  if (event.target == modal) {
    modal.style.display = "none";
  }
}